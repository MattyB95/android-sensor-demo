package com.mattyb95.androidsensordemo;

import org.jetbrains.annotations.NotNull;

import java.text.DecimalFormat;

class SensorData {

    private static final int SENSOR_VALUE_X = 0;
    private static final int SENSOR_VALUE_Y = 1;
    private static final int SENSOR_VALUE_Z = 2;
    private static final int THREE = 3;

    private final float sensorValue;
    private float xAxisValue;
    private float yAxisValue;
    private float zAxisValue;

    SensorData(@NotNull float[] sensorValues) {
        sensorValue = sensorValues[SENSOR_VALUE_X];
        if (sensorValues.length == THREE) {
            xAxisValue = sensorValues[SENSOR_VALUE_X];
            yAxisValue = sensorValues[SENSOR_VALUE_Y];
            zAxisValue = sensorValues[SENSOR_VALUE_Z];
        }
    }

    String getStringXAxisValue() {
        return getFormattedString(xAxisValue);
    }

    String getStringYAxisValue() {
        return getFormattedString(yAxisValue);
    }

    String getStringZAxisValue() {
        return getFormattedString(zAxisValue);
    }

    String getStringSensorValue() {
        return getFormattedString(sensorValue);
    }

    @NotNull
    private String getFormattedString(float sensorValue) {
        DecimalFormat decimalFormat = new DecimalFormat();
        decimalFormat.setMaximumFractionDigits(2);
        return decimalFormat.format(sensorValue);
    }

}
