package com.mattyb95.androidsensordemo;

import android.annotation.SuppressLint;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.jetbrains.annotations.NotNull;

import androidx.appcompat.app.AppCompatActivity;

public class SensorActivity extends AppCompatActivity implements SensorEventListener {

    private SensorHandler sensorHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sensor);

        sensorHandler = new SensorHandler(getApplicationContext());
        activateSensors();
    }

    @Override
    protected void onResume() {
        super.onResume();
        sensorHandler.registerListener(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        sensorHandler.unregisterListener(this);
    }

    @Override
    public void onSensorChanged(@NotNull SensorEvent event) {
        SensorData sensorData = new SensorData(event.values);
        switch (event.sensor.getType()) {
            // Motion Sensors
            case Sensor.TYPE_ACCELEROMETER:
                displayCoordinateSensorData(sensorData,
                        R.id.text_view_accelerometer_x,
                        R.id.text_view_accelerometer_y,
                        R.id.text_view_accelerometer_z);
                break;
            case Sensor.TYPE_ACCELEROMETER_UNCALIBRATED:
                displayCoordinateSensorData(sensorData,
                        R.id.text_view_accelerometer_uncalibrated_x,
                        R.id.text_view_accelerometer_uncalibrated_y,
                        R.id.text_view_accelerometer_uncalibrated_z);
                break;
            case Sensor.TYPE_GRAVITY:
                displayCoordinateSensorData(sensorData,
                        R.id.text_view_gravity_x,
                        R.id.text_view_gravity_y,
                        R.id.text_view_gravity_z);
                break;
            case Sensor.TYPE_GYROSCOPE:
                displayCoordinateSensorData(sensorData,
                        R.id.text_view_gyroscope_x,
                        R.id.text_view_gyroscope_y,
                        R.id.text_view_gyroscope_z);
                break;
            case Sensor.TYPE_GYROSCOPE_UNCALIBRATED:
                displayCoordinateSensorData(sensorData,
                        R.id.text_view_gyroscope_uncalibrated_x,
                        R.id.text_view_gyroscope_uncalibrated_y,
                        R.id.text_view_gyroscope_uncalibrated_z);
                break;
            case Sensor.TYPE_LINEAR_ACCELERATION:
                displayCoordinateSensorData(sensorData,
                        R.id.text_view_linear_acceleration_x,
                        R.id.text_view_linear_acceleration_y,
                        R.id.text_view_linear_acceleration_z);
                break;
            case Sensor.TYPE_ROTATION_VECTOR:
                displayCoordinateSensorData(sensorData,
                        R.id.text_view_rotation_vector_x,
                        R.id.text_view_rotation_vector_y,
                        R.id.text_view_rotation_vector_z);
                break;
            case Sensor.TYPE_STEP_COUNTER:
                displaySensorData(sensorData,
                        R.id.text_view_step_counter);
                break;

            // Position Sensors
            case Sensor.TYPE_GAME_ROTATION_VECTOR:
                displayCoordinateSensorData(sensorData,
                        R.id.text_view_game_rotation_vector_x,
                        R.id.text_view_game_rotation_vector_y,
                        R.id.text_view_game_rotation_vector_z);
                break;
            case Sensor.TYPE_GEOMAGNETIC_ROTATION_VECTOR:
                displayCoordinateSensorData(sensorData,
                        R.id.text_view_geomagnetic_rotation_vector_x,
                        R.id.text_view_geomagnetic_rotation_vector_y,
                        R.id.text_view_geomagnetic_rotation_vector_z);
                break;
            case Sensor.TYPE_MAGNETIC_FIELD:
                displayCoordinateSensorData(sensorData,
                        R.id.text_view_magnetic_field_x,
                        R.id.text_view_magnetic_field_y,
                        R.id.text_view_magnetic_field_z);
                break;
            case Sensor.TYPE_MAGNETIC_FIELD_UNCALIBRATED:
                displayCoordinateSensorData(sensorData,
                        R.id.text_view_magnetic_field_uncalibrated_x,
                        R.id.text_view_magnetic_field_uncalibrated_y,
                        R.id.text_view_magnetic_field_uncalibrated_z);
                break;
            case Sensor.TYPE_ORIENTATION:
                displayCoordinateSensorData(sensorData,
                        R.id.text_view_orientation_x,
                        R.id.text_view_orientation_y,
                        R.id.text_view_orientation_z);
                break;
            case Sensor.TYPE_PROXIMITY:
                displaySensorData(sensorData,
                        R.id.text_view_proximity);
                break;

            // Environment Sensors
            case Sensor.TYPE_AMBIENT_TEMPERATURE:
                displaySensorData(sensorData,
                        R.id.text_view_ambient_temperature);
                break;
            case Sensor.TYPE_LIGHT:
                displaySensorData(sensorData,
                        R.id.text_view_light);
                break;
            case Sensor.TYPE_PRESSURE:
                displaySensorData(sensorData,
                        R.id.text_view_pressure);
                break;
            case Sensor.TYPE_RELATIVE_HUMIDITY:
                displaySensorData(sensorData,
                        R.id.text_view_relative_humidity);
                break;
            case Sensor.TYPE_TEMPERATURE:
                displaySensorData(sensorData,
                        R.id.text_view_temperature);
                break;
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    @SuppressLint("InlinedApi")
    private void activateSensors() {
        // Motion Sensors
        sensorHandler.activateDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        sensorHandler.activateDefaultSensor(Sensor.TYPE_ACCELEROMETER_UNCALIBRATED);
        sensorHandler.activateDefaultSensor(Sensor.TYPE_GRAVITY);
        sensorHandler.activateDefaultSensor(Sensor.TYPE_GYROSCOPE);
        sensorHandler.activateDefaultSensor(Sensor.TYPE_GYROSCOPE_UNCALIBRATED);
        sensorHandler.activateDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION);
        sensorHandler.activateDefaultSensor(Sensor.TYPE_ROTATION_VECTOR);
        sensorHandler.activateDefaultSensor(Sensor.TYPE_STEP_COUNTER);

        // Position Sensors
        sensorHandler.activateDefaultSensor(Sensor.TYPE_GAME_ROTATION_VECTOR);
        sensorHandler.activateDefaultSensor(Sensor.TYPE_GEOMAGNETIC_ROTATION_VECTOR);
        sensorHandler.activateDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
        sensorHandler.activateDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD_UNCALIBRATED);
        sensorHandler.activateDefaultSensor(Sensor.TYPE_ORIENTATION);
        sensorHandler.activateDefaultSensor(Sensor.TYPE_PROXIMITY);

        // Environment Sensors
        sensorHandler.activateDefaultSensor(Sensor.TYPE_AMBIENT_TEMPERATURE);
        sensorHandler.activateDefaultSensor(Sensor.TYPE_LIGHT);
        sensorHandler.activateDefaultSensor(Sensor.TYPE_PRESSURE);
        sensorHandler.activateDefaultSensor(Sensor.TYPE_RELATIVE_HUMIDITY);
        sensorHandler.activateDefaultSensor(Sensor.TYPE_TEMPERATURE);
    }

    private void displaySensorData(@NotNull SensorData sensorData, int dataView) {
        TextView dataTextView = findViewById(dataView);
        dataTextView.setText(sensorData.getStringSensorValue());
    }

    private void displayCoordinateSensorData(@NotNull SensorData sensorData, int xView, int yView, int zView) {
        TextView xTextView = findViewById(xView);
        TextView yTextView = findViewById(yView);
        TextView zTextView = findViewById(zView);
        xTextView.setText(sensorData.getStringXAxisValue());
        yTextView.setText(sensorData.getStringYAxisValue());
        zTextView.setText(sensorData.getStringZAxisValue());
    }

    private boolean isSensorsActive(@NotNull Button sensorButton) {
        return sensorButton.getText().equals(getString(R.string.stop));
    }

    public void sensorControl(View view) {
        Button sensorButton = (Button) view;
        if (isSensorsActive(sensorButton)) {
            sensorHandler.unregisterListener(this);
            sensorButton.setText(R.string.start);
        } else {
            sensorHandler.registerListener(this);
            sensorButton.setText(getString(R.string.stop));
        }
    }

}
